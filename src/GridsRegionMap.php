<?php

namespace Drupal\grids;

use Drupal\grids\Plugin\views\row\Fields;

class GridsRegionMap {

  /**
   * @var \Drupal\grids\Plugin\views\row\Fields
   */
  private $plugin;

  /**
   * @var array
   */
  private $map;

  /**
   * @var \Drupal\grids\GridsFieldsOptions
   */
  private $fieldsOptions;

  public function __construct(Fields $plugin, GridsFieldsOptions $fields_options) {
    $this->plugin = $plugin;
    $this->fieldsOptions = $fields_options;
    $this->generateRegionMap();
  }

  /**
   * Generates the region map.
   */
  public function generateRegionMap() {
    $this->map = [];
    foreach ($this->plugin->view->field as $field_name => $field_definition) {
      $region_machine_name = $this->fieldHasValidAssignment($field_name) ? $this->fieldsOptions->getAssignedRegion($field_name) : $this->fieldsOptions->getDefaultRegion();
      $this->map[$region_machine_name][$field_name] = $field_definition;
    }
  }

  /**
   * Determines if the given field is assigned to an existing region.
   *
   * @param string $field_name
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function fieldHasValidAssignment($field_name) {
    return $this->selectedLayoutHasRegion($this->fieldsOptions->getAssignedRegion($field_name));
  }

  /**
   * Determines if the given machine name is a region in the selected layout.
   *
   * @param string $region_name
   *
   * @return bool
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function selectedLayoutHasRegion($region_name) {
    return in_array($region_name, $this->getRegionNamesForSelectedLayout());
  }

  /**
   * Gets the machine names of all regions in the selected layout.
   *
   * @return string[]
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getRegionNamesForSelectedLayout() {
    /** @var \Drupal\Core\Layout\LayoutDefinition $definition */
    $definition = $this->fieldsOptions->getSelectedLayoutDefinition();
    return $definition->getRegionNames();
  }

  /**
   * Retrieves an array of region machine names for all regions that contain
   * fields to be rendered.
   *
   * @return array
   */
  public function getNonEmptyRegionNames() {
    $non_empty = [];

    foreach ($this->map as $region => $fields) {
      if (!empty($fields)) {
        $non_empty[] = $region;
      }
    }

    return $non_empty;
  }

  /**
   * Returns the fields to be rendered for the given region.
   *
   * @param string $region_name
   *
   * @return \Drupal\views\Plugin\views\field\FieldPluginBase[]
   */
  public function getFieldsForRegion($region_name) {
    return !empty($this->map[$region_name]) ? $this->map[$region_name] : [];
  }
}
