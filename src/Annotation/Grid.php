<?php

namespace Drupal\grids\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\grids\GridDefinition;
use Drupal\grids\Plugin\Grids\GridDefault;

/**
 * Defines a Grid annotation object.
 *
 * Grids are used to define semantics for layouts according to grids' rules.
 *
 * Plugin namespace: Plugin\Grids
 *
 * @see \Drupal\grids\GridInterface
 * @see \Drupal\grids\GridDefault
 * @see \Drupal\grids\GridsPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class Grid extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name.
   *
   * @var string
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * An optional description for grids.
   *
   * @var string
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The human-readable category.
   *
   * @var string
   *
   * @see \Drupal\Component\Plugin\CategorizingPluginManagerInterface
   *
   * @ingroup plugin_translatable
   */
  public $category;

  /**
   * An associative array of configuration variables in this grid.
   *
   * @var array
   */
  public $configuration = [];

  /**
   * The grid plugin class.
   *
   * This default value is used for plugins defined in grids.yml that do not
   * specify a class themselves.
   *
   * @var string
   */
  public $class = GridDefault::class;

  /**
   * {@inheritdoc}
   */
  public function get() {
    return new GridDefinition($this->definition);
  }

}
