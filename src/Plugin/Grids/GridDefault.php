<?php

namespace Drupal\grids\Plugin\Grids;

/**
 * Provides a class for Default grid plugin.
 */
class GridDefault extends GridBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = parent::build();

    // Make your changes here.

    return $build;
  }

}
