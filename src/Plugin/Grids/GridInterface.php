<?php

namespace Drupal\grids\Plugin\Grids;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;

/**
 * Provides an interface for Grid plugins.
 */
interface GridInterface extends PluginInspectionInterface, DerivativeInspectionInterface, ConfigurableInterface, DependentPluginInterface {

  /**
   * Build a configuration array for the grid.
   *
   * @return array
   *   Configuration array for the grid.
   */
  public function build();

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\grids\GridDefinition
   */
  public function getPluginDefinition();

}
