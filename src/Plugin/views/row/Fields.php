<?php

namespace Drupal\grids\Plugin\views\row;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\views\Plugin\views\row\Fields as ViewsFields;
use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\grids\GridsFieldsOptions;
use Drupal\grids\GridsRegionMap;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The grids' 'fields' row plugin.
 * This displays fields in a grids layout.
 *
 * @ingroup views_row_plugins
 *
 * @ViewsRow(
 *   id = "grids_fields",
 *   title = @Translation("Grids fields"),
 *   help = @Translation("Displays the fields in a grids layout rather than using a simple row template."),
 *   theme = "views_view_fields",
 *   display_types = {"normal"}
 * )
 */
class Fields extends ViewsFields {

  /**
   * @var \Drupal\layout_plugin_views\RegionMap
   */
  private $regionMap;

  /**
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  private $layoutPluginManager;

  /**
   * @var \Drupal\grids\GridsFieldsOptions
   */
  private $fieldsOptions;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LayoutPluginManagerInterface $layout_plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->layoutPluginManager = $layout_plugin_manager;
    $this->fieldsOptions = GridsFieldsOptions::fromFieldsPlugin($layout_plugin_manager, $this);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.core.layout')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['layout']['default'] = '';
    $options['default_region']['default'] = '';

    $options['assigned_regions']['default'] = [];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    /** @var \Drupal\Core\Layout\LayoutDefinition $layout_definition */
    if ($this->fieldsOptions->hasValidSelectedLayout()) {
      $layout_definition = $this->fieldsOptions->getSelectedLayoutDefinition();
    }
    elseif ($this->fieldsOptions->layoutFallbackIsPossible()) {
      $layout_definition = $this->fieldsOptions->getFallbackLayoutDefinition();
    }

    if (!empty($layout_definition)) {
      $form['layout'] = [
        '#type' => 'select',
        '#title' => $this->t('Layout'),
        '#options' => $this->layoutPluginManager->getLayoutOptions(),
        '#default_value' => $layout_definition->id(),
      ];

      $labels = $layout_definition->getRegionLabels();

      $form['default_region'] = [
        '#type' => 'select',
        '#title' => $this->t('Default region'),
        '#description' => $this->t('Defines the region in which the fields will be rendered by default.'),
        '#options' => $labels,
        '#default_value' => $this->fieldsOptions->getDefaultRegion(),
      ];

      // If a layout is already selected, show the layout settings.
      $form['layout_settings'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Layout settings'),
        '#tree' => TRUE,
      ];

      $layout_settings = [];
      if (isset($this->options['layout_settings']['settings'])) {
        $layout_settings = $this->options['layout_settings']['settings'];
      }

      /** @var \Drupal\Core\Layout\LayoutDefinition $layout_definition */
      if ($this->fieldsOptions->hasValidSelectedLayout()) {
        $layout_definition = $this->fieldsOptions->getSelectedLayoutDefinition();
      }
      elseif ($this->fieldsOptions->layoutFallbackIsPossible()) {
        $layout_definition = $this->fieldsOptions->getFallbackLayoutDefinition();
      }

      $layout = $this->layoutPluginManager->createInstance($layout_definition->id(), $layout_settings);
      if ($layout instanceof PluginFormInterface) {
        $form['layout_settings']['settings'] = $layout->buildConfigurationForm([], $form_state);
      }

      $form['assigned_regions'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Assign regions'),
        '#description' => $this->t('You can use the dropdown menus above to select a region for each field to be rendered in.'),
      ];

      foreach ($this->displayHandler->getFieldLabels() as $field_name => $field_label) {
        $form['assigned_regions'][$field_name] = [
          '#type' => 'select',
          '#options' => $labels,
          '#title' => $field_label,
          '#default_value' => $this->fieldsOptions->getAssignedRegion($field_name),
          '#empty_option' => $this->t('Default region'),
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);

    /** @var \Drupal\Core\Layout\LayoutDefinition $layout_definition */
    if ($this->fieldsOptions->hasValidSelectedLayout()) {
      $layout_definition = $this->fieldsOptions->getSelectedLayoutDefinition();
    }
    elseif ($this->fieldsOptions->layoutFallbackIsPossible()) {
      $layout_definition = $this->fieldsOptions->getFallbackLayoutDefinition();
    }

    $layout_settings = isset($this->options['layout_settings']['settings']) ? $this->options['layout_settings']['settings'] : [];
    $layout = $this->layoutPluginManager->createInstance($layout_definition->id(), $layout_settings);
    if ($layout instanceof PluginFormInterface) {
      if ($configurations = $form_state->getValue(['row_options', 'layout_settings', 'settings', 'configurations'])) {
        $form_state->setValue('configurations', $configurations);
      }
      if ($configuration = $form_state->getValue(['row_options', 'layout_settings', 'settings', 'configuration'])) {
        $form_state->setValue('configuration', $configuration);
      }
      if ($grid = $form_state->getValue(['row_options', 'layout_settings', 'settings', 'grid'])) {
        $form_state->setValue('grid', $grid);
      }
      if ($grid_current = $form_state->getValue(['row_options', 'layout_settings', 'settings', 'grid_current'])) {
        $form_state->setValue('grid_current', $grid_current);
      }
      $layout->submitConfigurationForm($form, $form_state);
    }

    $this->options['layout_settings']['settings'] = $layout->getConfiguration();
    $form_state->setValue(['row_options', 'layout_settings', 'settings'], $layout->getConfiguration());
  }

  /**
   * {@inheritdoc}
   */
  public function render($row) {
    /** @var \Drupal\views\ResultRow $row */
    $build = $this->renderFieldsIntoRegions($row);
    return [
      '#context' => ['fields_plugin' => $this],
      '#theme' => 'grids_layout_fields',
      '#row' => $row,
      '#options' => $this->options,
      '#layout_fields' => $this->buildLayoutRenderArray($build),
    ];
  }

  /**
   * Renders the row's fields into the regions specified in the region map.
   *
   * @param \Drupal\views\ResultRow $row
   *
   * @return \Drupal\Component\Render\MarkupInterface[]
   *   An array of MarkupInterface objects keyed by region machine name.
   */
  protected function renderFieldsIntoRegions(ResultRow $row) {
    $build = [];
    foreach ($this->getRegionMap()->getNonEmptyRegionNames() as $region_name) {
      try {
        $fieldsToRender = $this->getRegionMap()->getFieldsForRegion($region_name);
        $build[$region_name]['#markup'] = $this->renderFields($row, $fieldsToRender);
      } catch (\RuntimeException $e) {
        // Even though we only try to render regions that actually contain
        // fields, it is still possible that those fields are empty. We don't
        // want to render empty regions, so we do nothing.
      }
    }

    return $build;
  }

  /**
   * Renders the fields.
   *
   * @param \Drupal\views\ResultRow $row
   * @param \Drupal\views\Plugin\views\field\FieldPluginBase[] $fieldsToRender
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *
   * @throws \RuntimeException
   */
  protected function renderFields(ResultRow $row, array $fieldsToRender) {
    // We have to override the available fields for rendering so we create a
    // backup of the original fields.
    $original_fields = $this->getViewFieldDefinitions();
    $this->setViewFieldDefinitions($fieldsToRender);

    // We can not just return a render array with a clone of a filtered view
    // because views assigns the view object just before rendering, which
    // results in all fields being rendered in each region.
    // We therefore have to force rendering outside of the render context of
    // this request.
    $renderer = $this->getRenderer();
    $markup = $renderer->executeInRenderContext(new RenderContext(), function () use ($row, $renderer) {
      // @codeCoverageIgnoreStart
      // We can never reach this code in our unit tests because we mocked out
      // the renderer. These two methods are however defined and tested by core.
      // There is no need for them to be tested by our unit tests.
      $render_array = parent::render($row);
      return $renderer->render($render_array);
      // @codeCoverageIgnoreEnd
    });

    // Restore the original fields.
    $this->setViewFieldDefinitions($original_fields);

    if (empty($markup)) {
      throw new \RuntimeException();
    }
    return $markup;
  }

  /**
   * Retrieves the field property of the view.
   *
   * @return \Drupal\views\Plugin\views\field\FieldPluginBase[]
   */
  protected function getViewFieldDefinitions() {
    return $this->view->field;
  }

  /**
   * Sets the field property of the view.
   *
   * @param \Drupal\views\Plugin\views\field\FieldPluginBase[] $fieldDefinitions
   */
  protected function setViewFieldDefinitions(array $fieldDefinitions) {
    $this->view->field = $fieldDefinitions;
  }

  /**
   * @return \Drupal\grids\GridsRegionMap
   */
  private function getRegionMap() {
    if (empty($this->regionMap)) {
      $this->regionMap = new GridsRegionMap($this, $this->fieldsOptions);
    }

    return $this->regionMap;
  }

  /**
   * Builds a renderable array for the selected layout.
   *
   * @param MarkupInterface[] $rendered_regions
   *  An array of MarkupInterface objects keyed by the machine name of the
   *  region they should be rendered in. @return array
   *  Renderable array for the selected layout.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @see ::renderFieldsIntoRegions.
   *
   */
  protected function buildLayoutRenderArray(array $rendered_regions) {
    if (!empty($rendered_regions)) {
      $layout_settings = [];
      if (isset($this->options['layout_settings']['settings'])) {
        $layout_settings = $this->options['layout_settings']['settings'];
      }
      /** @var \Drupal\Core\Layout\LayoutInterface $layout */
      $layout = $this->layoutPluginManager->createInstance($this->fieldsOptions->getLayout(), $layout_settings);
      return $layout->build($rendered_regions);
    }
    return $rendered_regions;
  }

}
