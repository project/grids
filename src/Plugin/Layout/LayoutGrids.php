<?php

namespace Drupal\grids\Plugin\Layout;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Template\Attribute;
use Drupal\grids\Form\GridConfigure;

/**
 * Layout class for all layouts with grids.
 */
class LayoutGrids extends LayoutDefault implements PluginFormInterface {

  /**
   * The Grids Plugin Manager.
   *
   * @var \Drupal\grids\GridsPluginManagerInterface
   */
  private $gridDiscovery;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->gridDiscovery = \Drupal::getContainer()->get('plugin.manager.grids');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'grid' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $grid_options = ['' => $this->t('- Select grid -')];
    $grid_options += $this->gridDiscovery->getGridLayoutOptions($this->pluginId);

    $form['grid'] = [
      '#type' => 'select',
      '#title' => $this->t('Grid'),
      '#options' => $grid_options,
      '#required' => TRUE,
      '#default_value' => $this->configuration['grid'],
    ];

    $form['grid_current'] = [
      '#type' => 'value',
      '#default_value' => $this->configuration['grid'],
    ];

    $form['configurations'] = [
      '#type' => 'value',
      '#default_value' => isset($this->configuration['configuration']) ? $this->configuration['configuration'] : [],
    ];

    // Grid configuration.
    $form['configuration'] = [
      '#type' => 'container',
      '#attributes' => ['id' => ['grid-configuration-wrapper']],
      '#tree' => TRUE,
    ];

    if (isset($this->configuration['grid']) && $this->configuration['grid']) {
      if (isset($this->configuration['configuration'][$this->configuration['grid']]) && $this->configuration['configuration'][$this->configuration['grid']]) {
        $grid_configuration = $this->configuration['configuration'][$this->configuration['grid']];
      }
      else {
        $grid_configuration = \Drupal::getContainer()->get('config.factory')->get("grids.{$this->pluginId}.{$this->configuration['grid']}")->get('configuration');
      }
      $form = array_merge($form, GridConfigure::buildFormConfiguration($grid_configuration));
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function massageConfigurationFormValues(array &$form, FormStateInterface $form_state) {
    if ($configuration = $form_state->getValue('configuration')) {
      $configuration['layout']['attributes']['class'] = _grids_text_to_array($configuration['layout']['attributes']['class']);
      $configuration['layout']['wrapper']['attributes']['class'] = _grids_text_to_array($configuration['layout']['wrapper']['attributes']['class']);
      $configuration['container']['attributes']['class'] = _grids_text_to_array($configuration['container']['attributes']['class']);
      $configuration['container']['wrapper']['attributes']['class'] = _grids_text_to_array($configuration['container']['wrapper']['attributes']['class']);

      foreach ($configuration['rows'] as &$row) {
        $row['attributes']['class'] = _grids_text_to_array($row['attributes']['class']);
        $row['wrapper']['attributes']['class'] = _grids_text_to_array($row['wrapper']['attributes']['class']);
      }

      foreach ($configuration['regions'] as &$region) {
        $region['attributes']['class'] = _grids_text_to_array($region['attributes']['class']);
        $region['wrapper']['attributes']['class'] = _grids_text_to_array($region['wrapper']['attributes']['class']);
      }
      $form_state->setValue('configuration', $configuration);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->massageConfigurationFormValues($form, $form_state);
    $this->configuration['grid'] = $form_state->getValue('grid');

    $this->configuration['configuration'] = $form_state->getValue('configurations');

    if ($configuration = $form_state->getValue('configuration')) {
      if ($form_state->getValue('grid') == $form_state->getValue('grid_current')) {
        $this->configuration['configuration'][$this->configuration['grid']] = $configuration;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);
    if (!isset($build['#settings']['grid']) || empty($build['#settings']['grid'])) {
      $build['#settings']['grid'] = 'default';
    }

    if (is_null($this->gridDiscovery)) {
      $this->gridDiscovery = \Drupal::getContainer()->get('plugin.manager.grids');
    }

    $build['#grid'] = $this->gridDiscovery->getDefinition($build['#settings']['grid']);

    $grid = isset($build['#settings']['configuration'][$build['#settings']['grid']]) ? $build['#settings']['configuration'][$build['#settings']['grid']] : $this->gridDiscovery->getLayoutGrid($build['#layout']->id(), $build['#grid']->id());

    // Process layout.
    $layout_attributes = new Attribute();
    $layout_class_prefix = '';
    if (isset($grid['layout']['class']) && $grid['layout']['class']) {
      $layout_attributes->addClass($grid['layout']['class']);
      $layout_class_prefix = $grid['layout']['class'] . '__';
    }
    if (isset($build['#layout']->get('additional')['grids']['type'])) {
      $layout_attributes->addClass($layout_class_prefix . $build['#layout']->get('additional')['grids']['type']);
      if (isset($build['#layout']->get('additional')['grids']['name'])) {
        $layout_attributes->addClass($layout_class_prefix . $build['#layout']->get('additional')['grids']['type'] . '-' . $build['#layout']->get('additional')['grids']['name']);
      }
    }

    if (is_array($grid['layout']['attributes']['class'])) {
      foreach ($grid['layout']['attributes']['class'] as $class) {
        if (!$layout_attributes->hasClass($class)) {
          $layout_attributes->addClass($class);
        }
      }
    }

    $grid['layout']['attributes'] = $layout_attributes;

    // Process layout wrapper.
    $layout_wrapper_attributes = new Attribute();
    $layout_wrapper_class_prefix = '';
    if (isset($grid['layout']['wrapper']['class']) && $grid['layout']['wrapper']['class']) {
      $layout_wrapper_attributes->addClass($grid['layout']['wrapper']['class']);
      $layout_wrapper_class_prefix = $grid['layout']['wrapper']['class'] . '__';
    }
    if (isset($build['#layout']->get('additional')['grids']['type'])) {
      $layout_wrapper_attributes->addClass($layout_wrapper_class_prefix . $build['#layout']->get('additional')['grids']['type']);
      if (isset($build['#layout']->get('additional')['grids']['name'])) {
        $layout_wrapper_attributes->addClass($layout_wrapper_class_prefix . $build['#layout']->get('additional')['grids']['type'] . '-' . $build['#layout']->get('additional')['grids']['name']);
      }
    }

    if (is_array($grid['layout']['wrapper']['attributes']['class'])) {
      foreach ($grid['layout']['wrapper']['attributes']['class'] as $class) {
        if (!$layout_wrapper_attributes->hasClass($class)) {
          $layout_wrapper_attributes->addClass($class);
        }
      }
    }

    $grid['layout']['wrapper']['attributes'] = $layout_wrapper_attributes;

    // Process container.
    $container_attributes = new Attribute();
    if (isset($grid['container']['class']) && $grid['container']['class']) {
      $container_attributes->addClass($grid['container']['class']);
    }

    if (is_array($grid['container']['attributes']['class'])) {
      foreach ($grid['container']['attributes']['class'] as $class) {
        if (!$container_attributes->hasClass($class)) {
          $container_attributes->addClass($class);
        }
      }
    }

    $grid['container']['attributes'] = $container_attributes;

    // Process container wrapper.
    $container_wrapper_attributes = new Attribute();
    if (isset($grid['container']['wrapper']['class']) && $grid['container']['wrapper']['class']) {
      $container_wrapper_attributes->addClass($grid['container']['wrapper']['class']);
    }

    if (is_array($grid['container']['wrapper']['attributes']['class'])) {
      foreach ($grid['container']['wrapper']['attributes']['class'] as $class) {
        if (!$container_wrapper_attributes->hasClass($class)) {
          $container_wrapper_attributes->addClass($class);
        }
      }
    }

    $grid['container']['wrapper']['attributes'] = $container_wrapper_attributes;

    // Process rows.
    foreach ($grid['rows'] as $row_id => &$row) {
      // Process row.
      $row_attributes = new Attribute();
      if (isset($row['class']) && $row['class']) {
        $row_attributes->addClass($layout_class_prefix . $row['class']);
        $row_attributes->addClass($layout_class_prefix . $row['class'] . '--' . $row_id);
        $row_attributes->addClass($row['class']);
      }

      if (is_array($row['attributes']['class'])) {
        foreach ($row['attributes']['class'] as $class) {
          if (!$row_attributes->hasClass($class)) {
            $row_attributes->addClass($class);
          }
        }
      }

      $row['attributes'] = $row_attributes;

      // Process row wrapper.
      $row_wrapper_attributes = new Attribute();
      if (isset($row['wrapper']['class']) && $row['wrapper']['class']) {
        $row_wrapper_attributes->addClass($row['wrapper']['class']);
      }

      if (is_array($row['wrapper']['attributes']['class'])) {
        foreach ($row['wrapper']['attributes']['class'] as $class) {
          if (!$row_wrapper_attributes->hasClass($class)) {
            $row_wrapper_attributes->addClass($class);
          }
        }
      }

      $row['wrapper']['attributes'] = $row_wrapper_attributes;
    }

    // Process regions.
    foreach ($grid['regions'] as $region_id => &$region) {
      // Process region.
      $region_attributes = new Attribute();
      if (isset($region['class']) && $region['class']) {
        $region_attributes->addClass($layout_class_prefix . $region['class']);
        $region_attributes->addClass($layout_class_prefix . $region['class'] . '--' . $region_id);
        $region_attributes->addClass($region['class']);
      }

      if (is_array($region['attributes']['class'])) {
        foreach ($region['attributes']['class'] as $class) {
          if (!$region_attributes->hasClass($class)) {
            $region_attributes->addClass($class);
          }
        }
      }

      $region['attributes'] = $region_attributes;

      // Process region wrapper.
      $region_wrapper_attributes = new Attribute();
      if (isset($region['wrapper']['class']) && $region['wrapper']['class']) {
        $region_wrapper_attributes->addClass($region['wrapper']['class']);
      }

      if (is_array($region['wrapper']['attributes']['class'])) {
        foreach ($region['wrapper']['attributes']['class'] as $class) {
          if (!$region_wrapper_attributes->hasClass($class)) {
            $region_wrapper_attributes->addClass($class);
          }
        }
      }

      $region['wrapper']['attributes'] = $region_wrapper_attributes;
    }

    // Add grid to settings and remove configuration.
    $build['#settings']['grid'] = $grid;
    unset($build['#settings']['configuration']);

    return $build;
  }


}
