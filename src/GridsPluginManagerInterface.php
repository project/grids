<?php

namespace Drupal\grids;

use Drupal\Component\Plugin\CategorizingPluginManagerInterface;
use Drupal\Core\Layout\LayoutDefinition;
use Drupal\Core\Plugin\FilteredPluginManagerInterface;

/**
 * Provides the interface for a plugin manager of grids.
 */
interface GridsPluginManagerInterface extends CategorizingPluginManagerInterface, FilteredPluginManagerInterface {

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\grids\Plugin\Grids\GridInterface
   */
  public function createInstance($plugin_id, array $configuration = []);

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\grids\GridDefinition|null
   */
  public function getDefinition($plugin_id, $exception_on_invalid = TRUE);

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\grids\GridDefinition[]
   */
  public function getDefinitions();

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\grids\GridDefinition[]
   */
  public function getSortedDefinitions(array $definitions = NULL);

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\grids\GridDefinition[][]
   */
  public function getGroupedDefinitions(array $definitions = NULL);

  /**
   * Resets the grid for specific layout.
   *
   * @param \Drupal\Core\Layout\LayoutDefinition $layout_definition
   * @param \Drupal\grids\GridDefinition $grid_definition
   */
  public function resetLayoutGrid(LayoutDefinition $layout_definition, GridDefinition $grid_definition);

  /**
   * Returns the configuration of the grid for specific layout.
   *
   * @param string $layout
   * @param string $grid
   *
   * @return array
   *   An array of configuration values.
   */
  public function getLayoutGrid($layout, $grid);

  /**
   * Deletes the grid for specific layout.
   *
   * @param string $layout
   * @param string $grid
   */
  public function deleteLayoutGrid($layout, $grid);

  /**
   * Generates default grid configuration for specific layout.
   *
   * @param \Drupal\Core\Layout\LayoutDefinition $layout_definition
   * @param \Drupal\grids\GridDefinition $grid_definition
   */
  public function defaultLayoutGridConfiguration(LayoutDefinition $layout_definition, GridDefinition $grid_definition);

  /**
   * Returns an array of grid labels grouped by category.
   *
   * @return string[][]
   *   A nested array of labels suitable for #options.
   */
  public function getGridOptions();

  /**
   * Returns an array of grid labels what are enabled for specific layout.
   *
   * @param $layout_id
   *   Layout machine name.
   *
   * @return string[]
   *   A nested array of labels suitable for #options.
   */
  public function getGridLayoutOptions($layout_id);

  /**
   * Returns an array of grid-able layouts.
   *
   * @return \Drupal\Core\Layout\LayoutDefinition[]
   *   An array of layout definitions.
   */
  public function getGridLayouts();

}
