<?php

namespace Drupal\grids;

use Drupal\Component\Annotation\Plugin\Discovery\AnnotationBridgeDecorator;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Layout\LayoutDefinition;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\CategorizingPluginManagerTrait;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Discovery\AnnotatedClassDiscovery;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscoveryDecorator;
use Drupal\Core\Plugin\FilteredPluginManagerTrait;
use Drupal\grids\Annotation\Grid;
use Drupal\grids\Plugin\Grids\GridInterface;
use Drupal\grids\Plugin\Layout\LayoutGrids;

/**
 * Provides a plugin manager for grids.
 */
class GridsPluginManager extends DefaultPluginManager implements GridsPluginManagerInterface {

  use FilteredPluginManagerTrait;
  use CategorizingPluginManagerTrait;

  /**
   * The layout discovery.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutDiscovery;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * GridsPluginManager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Layout\LayoutPluginManagerInterface $layout_discovery
   *   Layout discovery.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, LayoutPluginManagerInterface $layout_discovery , ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, ThemeHandlerInterface $theme_handler) {
    parent::__construct('Plugin/Grids', $namespaces, $module_handler, GridInterface::class, Grid::class);
    $this->layoutDiscovery = $layout_discovery;
    $this->configFactory = $config_factory;
    $this->themeHandler = $theme_handler;

    $type = $this->getType();
    $this->setCacheBackend($cache_backend, $type);
    $this->alterInfo($type);
  }

  /**
   * {@inheritdoc}
   */
  protected function getType() {
    return 'grid';
  }

  /**
   * {@inheritdoc}
   */
  protected function providerExists($provider) {
    return $this->moduleHandler->moduleExists($provider) || $this->themeHandler->themeExists($provider);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDiscovery() {
    if (!$this->discovery) {
      $discovery = new AnnotatedClassDiscovery($this->subdir, $this->namespaces, $this->pluginDefinitionAnnotationName, $this->additionalAnnotationNamespaces);
      $discovery = new YamlDiscoveryDecorator($discovery, 'grids', $this->moduleHandler->getModuleDirectories() + $this->themeHandler->getThemeDirectories());
      $discovery
        ->addTranslatableProperty('label')
        ->addTranslatableProperty('description')
        ->addTranslatableProperty('category');
      $discovery = new AnnotationBridgeDecorator($discovery, $this->pluginDefinitionAnnotationName);
      $discovery = new ContainerDerivativeDiscoveryDecorator($discovery);
      $this->discovery = $discovery;
    }
    return $this->discovery;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition($plugin_id, $exception_on_invalid = TRUE) {
    $definition = parent::getDefinition($plugin_id, $exception_on_invalid);

    return $definition;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinitions() {
    $definitions = parent::getDefinitions();

    // Delete all missing grid configs.
    $config_names = $this->configFactory->listAll('grids.grid');
    foreach ($config_names as $config_name) {
      list(,,$grid_name) = explode('.', $config_name);
      if (!array_key_exists($grid_name, $definitions)) {
        $this->configFactory->getEditable($config_name)->delete();
      }
    }

    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    if (!$definition instanceof GridDefinition) {
      throw new InvalidPluginDefinitionException($plugin_id, sprintf('The "%s" grid definition must extend %s', $plugin_id, GridDefinition::class));
    }

    // Create or update default grid's config if it's new or not identical.
    if ($plugin_id == 'default') {
      foreach ($this->getGridLayouts() as $layout_definition) {
        $layout_grid_configuration = $this->configFactory->get("grids.{$layout_definition->id()}.$plugin_id")->getRawData();
        if (!$layout_grid_configuration) {
          $this->resetLayoutGrid($layout_definition, $definition);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function resetLayoutGrid(LayoutDefinition $layout_definition, GridDefinition $grid_definition) {
    $layout_grid_configuration = $this->configFactory->getEditable("grids.{$layout_definition->id()}.{$grid_definition->id()}");
    $grid_configuration = $this->defaultLayoutGridConfiguration($layout_definition, $grid_definition);
    $layout_grid_configuration->set('configuration', $grid_configuration);
    $layout_grid_configuration->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getLayoutGrid($layout, $grid) {
    return $this->configFactory->get("grids.{$layout}.{$grid}")->get('configuration');
  }

  /**
   * {@inheritdoc}
   */
  public function deleteLayoutGrid($layout, $grid) {
    $this->configFactory->getEditable("grids.{$layout}.{$grid}")->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultLayoutGridConfiguration(LayoutDefinition $layout_definition, GridDefinition $grid_definition) {
    $default_configuration = $grid_definition->getConfiguration();
    $grid_configuration = [];
    $grid_configuration['layout'] = $default_configuration['layout'];
    $grid_configuration['container'] = $default_configuration['container'];

    foreach ($layout_definition->get('additional')['grids']['rows'] as $row_id => $row) {
      $grid_configuration['rows'][$row_id] = $default_configuration['row'];
      $grid_configuration['rows'][$row_id]['label'] = (string) $layout_definition->get('additional')['grids']['rows'][$row_id]['label'];
    }
    foreach ($layout_definition->get('regions') as $region_id => $region) {
      $grid_configuration['regions'][$region_id] = $default_configuration['region'];
      $grid_configuration['regions'][$region_id]['label'] = (string) $layout_definition->getRegions()[$region_id]['label'];
    }
    return $grid_configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function getGridOptions() {
    $grid_options = [];
    foreach ($this->getGroupedDefinitions() as $category => $grid_definitions) {
      foreach ($grid_definitions as $name => $grid_definition) {
        $grid_options[$category][$name] = $grid_definition->getLabel();
      }
    }
    return $grid_options;
  }

  /**
   * {@inheritdoc}
   */
  public function getGridLayoutOptions($layout_id) {
    $grid_options = [];
    foreach ($this->getDefinitions() as $name => $grid_definition) {
      $config = $this->configFactory->get("grids.{$layout_id}.{$grid_definition->id()}");
      if (!$config->isNew()) {
        $grid_options[$name] = $grid_definition->getLabel();
      }
    }
    return $grid_options;
  }


  /**
   * {@inheritdoc}
   */
  public function getGridLayouts() {
    $grid_layouts = [];
    foreach ($this->layoutDiscovery->getDefinitions() as $layout_definition) {
      if ($layout_definition->getClass() == LayoutGrids::class) {
        $grid_layouts[$layout_definition->id()] = $layout_definition;
      }
    }
    return $grid_layouts;
  }

}
