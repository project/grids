<?php

namespace Drupal\grids\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Url;
use Drupal\grids\GridsPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Grid configure form.
 */
class GridConfigure extends ConfigFormBase {

  /**
   * The layout discovery.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  private $layoutDiscovery;

  /**
   * The Grids Plugin Manager.
   *
   * @var \Drupal\grids\GridsPluginManagerInterface
   */
  private $gridDiscovery;

  /**
   * Layout ID.
   *
   * @var string
   */
  private $layout;

  /**
   * Grid ID.
   *
   * @var string
   */
  private $grid;

  /**
   * {@inheritdoc}
   */
  public function __construct(LayoutPluginManagerInterface $layout_discovery, GridsPluginManagerInterface $grid_discovery) {
    $this->layoutDiscovery = $layout_discovery;
    $this->gridDiscovery = $grid_discovery;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.core.layout'),
      $container->get('plugin.manager.grids')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'grid_configure_form';
  }

  /**
   * Builds the grid configuration form.
   *
   * @param $grid_configuration
   *
   * @return array
   */
  public static function buildFormConfiguration($grid_configuration = NULL) {
    if (!$grid_configuration) {
      return [];
    }

    // Grid configuration.
    $form['configuration'] = [
      '#type' => 'container',
      '#attributes' => ['id' => ['grid-configuration-wrapper']],
      '#tree' => TRUE,
    ];

    // Layout configuration.
    $form['configuration']['layout'] = [
      '#type' => 'details',
      '#title' => t('Layout'),
      '#group' => 'configuration',
      '#tree' => TRUE,
    ];
    $form['configuration']['layout']['enable'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable layout'),
      '#default_value' => isset($grid_configuration['layout']['enable']) ? $grid_configuration['layout']['enable'] : 0,
    ];
    $form['configuration']['layout']['tag'] = [
      '#type' => 'textfield',
      '#title' => t('Tag'),
      '#default_value' => isset($grid_configuration['layout']['tag']) ? $grid_configuration['layout']['tag'] : 'div',
      '#description' => t('The tag for layout.'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[layout][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['layout']['class'] = [
      '#type' => 'textfield',
      '#title' => t('Class'),
      '#default_value' => isset($grid_configuration['layout']['class']) ? $grid_configuration['layout']['class'] : '',
      '#description' => t('The main class for layout.'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[layout][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['layout']['attributes'] = [
      '#type' => 'fieldset',
      '#title' => t('Attributes'),
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="configuration[layout][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['layout']['attributes']['class'] = [
      '#type' => 'textarea',
      '#title' => t('Class'),
      '#default_value' => isset($grid_configuration['layout']['attributes']['class']) ? implode(" ", $grid_configuration['layout']['attributes']['class']) : '',
      '#description' => t('The class attribute for layout. One class per line.'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[layout][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['layout']['wrapper'] = [
      '#type' => 'fieldset',
      '#title' => t('Layout Wrapper'),
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="configuration[layout][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['layout']['wrapper']['enable'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable layout wrapper'),
      '#default_value' => isset($grid_configuration['layout']['wrapper']['enable']) ? $grid_configuration['layout']['wrapper']['enable'] : 0,
      '#states' => [
        'visible' => [
          ':input[name="configuration[layout][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['layout']['wrapper']['tag'] = [
      '#type' => 'textfield',
      '#title' => t('Wrapper Tag'),
      '#default_value' => isset($grid_configuration['layout']['wrapper']['tag']) ? $grid_configuration['layout']['wrapper']['tag'] : 'div',
      '#description' => t('The tag for layout wrapper.'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[layout][wrapper][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['layout']['wrapper']['class'] = [
      '#type' => 'textfield',
      '#title' => t('Wrapper Class'),
      '#default_value' => isset($grid_configuration['layout']['wrapper']['class']) ? $grid_configuration['layout']['wrapper']['class'] : '',
      '#description' => t('The main class for layout wrapper.'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[layout][wrapper][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['layout']['wrapper']['attributes'] = [
      '#type' => 'fieldset',
      '#title' => t('Wrapper Attributes'),
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="configuration[layout][wrapper][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['layout']['wrapper']['attributes']['class'] = [
      '#type' => 'textarea',
      '#title' => t('Class'),
      '#default_value' => isset($grid_configuration['layout']['wrapper']['attributes']['class']) ? implode(" ", $grid_configuration['layout']['wrapper']['attributes']['class']) : '',
      '#description' => t('The class attribute for layout wrapper. One class per line.'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[layout][wrapper][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Container configuration.
    $form['configuration']['container'] = [
      '#type' => 'details',
      '#title' => t('Container'),
      '#group' => 'configuration',
      '#tree' => TRUE,
    ];
    $form['configuration']['container']['enable'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable container'),
      '#default_value' => isset($grid_configuration['container']['enable']) ? $grid_configuration['container']['enable'] : 0,
    ];
    $form['configuration']['container']['tag'] = [
      '#type' => 'textfield',
      '#title' => t('Tag'),
      '#default_value' => isset($grid_configuration['container']['tag']) ? $grid_configuration['container']['tag'] : 'div',
      '#description' => t('The tag for container.'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[container][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['container']['class'] = [
      '#type' => 'textfield',
      '#title' => t('Class'),
      '#default_value' => isset($grid_configuration['container']['class']) ? $grid_configuration['container']['class'] : '',
      '#description' => t('The main class for container.'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[container][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['container']['attributes'] = [
      '#type' => 'fieldset',
      '#title' => t('Attributes'),
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="configuration[container][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['container']['attributes']['class'] = [
      '#type' => 'textarea',
      '#title' => t('Class'),
      '#default_value' => isset($grid_configuration['container']['attributes']['class']) ? implode(" ", $grid_configuration['container']['attributes']['class']) : '',
      '#description' => t('The class attribute for container. One class per line.'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[container][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['container']['wrapper'] = [
      '#type' => 'fieldset',
      '#title' => t('Container Wrapper'),
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="configuration[container][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['container']['wrapper']['enable'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable container wrapper'),
      '#default_value' => isset($grid_configuration['container']['wrapper']['enable']) ? $grid_configuration['container']['wrapper']['enable'] : 0,
      '#states' => [
        'visible' => [
          ':input[name="configuration[container][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['container']['wrapper']['tag'] = [
      '#type' => 'textfield',
      '#title' => t('Wrapper Tag'),
      '#default_value' => isset($grid_configuration['container']['wrapper']['tag']) ? $grid_configuration['container']['wrapper']['tag'] : 'div',
      '#description' => t('The tag for container wrapper.'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[container][wrapper][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['container']['wrapper']['class'] = [
      '#type' => 'textfield',
      '#title' => t('Wrapper Class'),
      '#default_value' => isset($grid_configuration['container']['wrapper']['class']) ? $grid_configuration['container']['wrapper']['class'] : '',
      '#description' => t('The main class for container wrapper.'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[container][wrapper][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['container']['wrapper']['attributes'] = [
      '#type' => 'fieldset',
      '#title' => t('Wrapper Attributes'),
      '#tree' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="configuration[container][wrapper][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['configuration']['container']['wrapper']['attributes']['class'] = [
      '#type' => 'textarea',
      '#title' => t('Class'),
      '#default_value' => isset($grid_configuration['container']['wrapper']['attributes']['class']) ? implode(" ", $grid_configuration['container']['wrapper']['attributes']['class']) : '',
      '#description' => t('The class attribute for container wrapper. One class per line.'),
      '#states' => [
        'visible' => [
          ':input[name="configuration[container][wrapper][enable]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    // Rows configuration.
    $form['configuration']['rows'] = [
      '#type' => 'details',
      '#title' => t('Rows'),
      '#group' => 'configuration',
      '#tree' => TRUE,
    ];
    foreach ($grid_configuration['rows'] as $row_id => $row) {
      $form['configuration']['rows'][$row_id] = [
        '#type' => 'details',
        '#title' => t('Row: ' . $row['label']),
        '#group' => 'rows',
        '#tree' => TRUE,
      ];
      $form['configuration']['rows'][$row_id]['label'] = [
        '#type' => 'value',
        '#default_value' => isset($grid_configuration['rows'][$row_id]['label']) ? $grid_configuration['rows'][$row_id]['label'] : '',
      ];
      $form['configuration']['rows'][$row_id]['enable'] = [
        '#type' => 'checkbox',
        '#title' => t('Enable row'),
        '#default_value' => isset($grid_configuration['rows'][$row_id]['enable']) ? $grid_configuration['rows'][$row_id]['enable'] : 0,
      ];
      $form['configuration']['rows'][$row_id]['tag'] = [
        '#type' => 'textfield',
        '#title' => t('Tag'),
        '#default_value' => isset($grid_configuration['rows'][$row_id]['tag']) ? $grid_configuration['rows'][$row_id]['tag'] : 'div',
        '#description' => t('The tag for row.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[rows]['.$row_id.'][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['rows'][$row_id]['class'] = [
        '#type' => 'textfield',
        '#title' => t('Class'),
        '#default_value' => isset($grid_configuration['rows'][$row_id]['class']) ? $grid_configuration['rows'][$row_id]['class'] : '',
        '#description' => t('The main class for row.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[rows]['.$row_id.'][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['rows'][$row_id]['attributes'] = [
        '#type' => 'fieldset',
        '#title' => t('Attributes'),
        '#tree' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="configuration[rows]['.$row_id.'][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['rows'][$row_id]['attributes']['class'] = [
        '#type' => 'textarea',
        '#title' => t('Class'),
        '#default_value' => isset($grid_configuration['rows'][$row_id]['attributes']['class']) ? implode(" ", $grid_configuration['rows'][$row_id]['attributes']['class']) : '',
        '#description' => t('The class attribute for row. One class per line.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[rows]['.$row_id.'][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['rows'][$row_id]['wrapper'] = [
        '#type' => 'fieldset',
        '#title' => t('Row Wrapper'),
        '#tree' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="configuration[rows]['.$row_id.'][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['rows'][$row_id]['wrapper']['enable'] = [
        '#type' => 'checkbox',
        '#title' => t('Enable row wrapper'),
        '#default_value' => isset($grid_configuration['rows'][$row_id]['wrapper']['enable']) ? $grid_configuration['rows'][$row_id]['wrapper']['enable'] : 0,
        '#states' => [
          'visible' => [
            ':input[name="configuration[rows]['.$row_id.'][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['rows'][$row_id]['wrapper']['tag'] = [
        '#type' => 'textfield',
        '#title' => t('Wrapper Tag'),
        '#default_value' => isset($grid_configuration['rows'][$row_id]['wrapper']['tag']) ? $grid_configuration['rows'][$row_id]['wrapper']['tag'] : 'div',
        '#description' => t('The tag for row wrapper.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[rows]['.$row_id.'][wrapper][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['rows'][$row_id]['wrapper']['class'] = [
        '#type' => 'textfield',
        '#title' => t('Wrapper Class'),
        '#default_value' => isset($grid_configuration['rows'][$row_id]['wrapper']['class']) ? $grid_configuration['rows'][$row_id]['wrapper']['class'] : '',
        '#description' => t('The main class for row wrapper.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[rows]['.$row_id.'][wrapper][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['rows'][$row_id]['wrapper']['attributes'] = [
        '#type' => 'fieldset',
        '#title' => t('Wrapper Attributes'),
        '#tree' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="configuration[rows]['.$row_id.'][wrapper][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['rows'][$row_id]['wrapper']['attributes']['class'] = [
        '#type' => 'textarea',
        '#title' => t('Class'),
        '#default_value' => isset($grid_configuration['rows'][$row_id]['wrapper']['attributes']['class']) ? implode(" ", $grid_configuration['rows'][$row_id]['wrapper']['attributes']['class']) : '',
        '#description' => t('The class attribute for row wrapper. One class per line.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[rows]['.$row_id.'][wrapper][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    // Regions configuration.
    $form['configuration']['regions'] = [
      '#type' => 'details',
      '#title' => t('Regions'),
      '#group' => 'configuration',
      '#tree' => TRUE,
    ];
    foreach ($grid_configuration['regions'] as $region_id => $region) {
      $form['configuration']['regions'][$region_id] = [
        '#type' => 'details',
        '#title' => t('Region: ' . (string)$region['label']),
        '#group' => 'regions',
        '#tree' => TRUE,
      ];
      $form['configuration']['regions'][$region_id]['label'] = [
        '#type' => 'value',
        '#default_value' => isset($grid_configuration['regions'][$region_id]['label']) ? $grid_configuration['regions'][$region_id]['label'] : '',
      ];
      $form['configuration']['regions'][$region_id]['enable'] = [
        '#type' => 'checkbox',
        '#title' => t('Enable region'),
        '#default_value' => isset($grid_configuration['regions'][$region_id]['enable']) ? $grid_configuration['regions'][$region_id]['enable'] : 0,
      ];
      $form['configuration']['regions'][$region_id]['tag'] = [
        '#type' => 'textfield',
        '#title' => t('Tag'),
        '#default_value' => isset($grid_configuration['regions'][$region_id]['tag']) ? $grid_configuration['regions'][$region_id]['tag'] : 'div',
        '#description' => t('The tag for region.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[regions]['.$region_id.'][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['regions'][$region_id]['class'] = [
        '#type' => 'textfield',
        '#title' => t('Class'),
        '#default_value' => isset($grid_configuration['regions'][$region_id]['class']) ? $grid_configuration['regions'][$region_id]['class'] : '',
        '#description' => t('The main class for region.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[regions]['.$region_id.'][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['regions'][$region_id]['attributes'] = [
        '#type' => 'fieldset',
        '#title' => t('Attributes'),
        '#tree' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="configuration[regions]['.$region_id.'][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['regions'][$region_id]['attributes']['class'] = [
        '#type' => 'textarea',
        '#title' => t('Class'),
        '#default_value' => isset($grid_configuration['regions'][$region_id]['attributes']['class']) ? implode(" ", $grid_configuration['regions'][$region_id]['attributes']['class']) : '',
        '#description' => t('The class attribute for region. One class per line.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[regions]['.$region_id.'][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['regions'][$region_id]['wrapper'] = [
        '#type' => 'fieldset',
        '#title' => t('Region Wrapper'),
        '#tree' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="configuration[regions]['.$region_id.'][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['regions'][$region_id]['wrapper']['enable'] = [
        '#type' => 'checkbox',
        '#title' => t('Enable region wrapper'),
        '#default_value' => isset($grid_configuration['regions'][$region_id]['wrapper']['enable']) ? $grid_configuration['regions'][$region_id]['wrapper']['enable'] : 0,
        '#states' => [
          'visible' => [
            ':input[name="configuration[regions]['.$region_id.'][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['regions'][$region_id]['wrapper']['tag'] = [
        '#type' => 'textfield',
        '#title' => t('Wrapper Tag'),
        '#default_value' => isset($grid_configuration['regions'][$region_id]['wrapper']['tag']) ? $grid_configuration['regions'][$region_id]['wrapper']['tag'] : 'div',
        '#description' => t('The tag for region wrapper.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[regions]['.$region_id.'][wrapper][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['regions'][$region_id]['wrapper']['class'] = [
        '#type' => 'textfield',
        '#title' => t('Wrapper Class'),
        '#default_value' => isset($grid_configuration['regions'][$region_id]['wrapper']['class']) ? $grid_configuration['regions'][$region_id]['wrapper']['class'] : '',
        '#description' => t('The main class for region wrapper.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[regions]['.$region_id.'][wrapper][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['regions'][$region_id]['wrapper']['attributes'] = [
        '#type' => 'fieldset',
        '#title' => t('Wrapper Attributes'),
        '#tree' => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="configuration[regions]['.$region_id.'][wrapper][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['configuration']['regions'][$region_id]['wrapper']['attributes']['class'] = [
        '#type' => 'textarea',
        '#title' => t('Class'),
        '#default_value' => isset($grid_configuration['regions'][$region_id]['wrapper']['attributes']['class']) ? implode(" ", $grid_configuration['regions'][$region_id]['wrapper']['attributes']['class']) : '',
        '#description' => t('The class attribute for region wrapper. One class per line.'),
        '#states' => [
          'visible' => [
            ':input[name="configuration[regions]['.$region_id.'][wrapper][enable]"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $layout = NULL, $grid = NULL) {
    if (is_null($layout) || is_null($grid)) {
      throw new NotFoundHttpException();
    }

    $this->layout = $layout;
    $layout_definition = $this->layoutDiscovery->getDefinition($layout);

    $this->grid = $grid;
    $grid_definition = $this->gridDiscovery->getDefinition($grid);

    $grid_configuration = $this->config("grids.$layout.$grid")->get('configuration');
    if (!$grid_configuration) {
      $grid_configuration = $this->gridDiscovery->defaultLayoutGridConfiguration($layout_definition, $grid_definition);
    }

    $form = self::buildFormConfiguration($grid_configuration);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $configuration = $form_state->getValue('configuration');
    $configuration['layout']['attributes']['class'] = _grids_text_to_array($configuration['layout']['attributes']['class']);
    $configuration['layout']['wrapper']['attributes']['class'] = _grids_text_to_array($configuration['layout']['wrapper']['attributes']['class']);
    $configuration['container']['attributes']['class'] = _grids_text_to_array($configuration['container']['attributes']['class']);
    $configuration['container']['wrapper']['attributes']['class'] = _grids_text_to_array($configuration['container']['wrapper']['attributes']['class']);

    foreach ($configuration['rows'] as &$row) {
      $row['attributes']['class'] = _grids_text_to_array($row['attributes']['class']);
      $row['wrapper']['attributes']['class'] = _grids_text_to_array($row['wrapper']['attributes']['class']);
    }

    foreach ($configuration['regions'] as &$region) {
      $region['attributes']['class'] = _grids_text_to_array($region['attributes']['class']);
      $region['wrapper']['attributes']['class'] = _grids_text_to_array($region['wrapper']['attributes']['class']);
    }
    $form_state->setValue('configuration', $configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $configuration = $form_state->getValue('configuration');
    $grid_configuration = $this->config("grids.{$this->layout}.{$this->grid}");
    $grid_configuration->set('configuration', $configuration);
    $grid_configuration->save();

    $this->messenger()->addStatus(t('<em>@grid</em> grid for <em>@layout</em> layout has been updated.', ['@grid' => $this->grid, '@layout' => $this->layout]));
  }

  /**
   * Builds the cancel link url for the form.
   *
   * @return Url
   *   Cancel url
   */
  private function buildCancelLinkUrl() {
    $query = $this->getRequest()->query;

    if ($query->has('destination')) {
      $options = UrlHelper::parse($query->get('destination'));
      $url = Url::fromUri('internal:/' . $options['path'], $options);
    }
    else {
      $url = Url::fromRoute('grids.grid.list');
    }

    return $url;
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return [
      "grids.{$this->layout}.{$this->grid}",
    ];
  }

}
