<?php

namespace Drupal\grids\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Url;
use Drupal\grids\GridsPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Grid reset form.
 */
class GridReset extends ConfirmFormBase {

  /**
   * The layout discovery.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  private $layoutDiscovery;

  /**
   * The Grids Plugin Manager.
   *
   * @var \Drupal\grids\GridsPluginManagerInterface
   */
  private $gridDiscovery;

  /**
   * Layout ID.
   *
   * @var string
   */
  private $layout;

  /**
   * Grid ID.
   *
   * @var string
   */
  private $grid;

  /**
   * {@inheritdoc}
   */
  public function __construct(LayoutPluginManagerInterface $layout_discovery, GridsPluginManagerInterface $grid_discovery) {
    $this->layoutDiscovery = $layout_discovery;
    $this->gridDiscovery = $grid_discovery;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.core.layout'),
      $container->get('plugin.manager.grids')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'grid_reset_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to reset %grid grid for %layout layout ?', ['%grid' => $this->grid, '%layout' => $this->layout]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Reset');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('grids.grid.list');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $layout = NULL, $grid = NULL) {
    if (is_null($layout) || is_null($grid)) {
      throw new NotFoundHttpException();
    }

    $this->layout = $layout;
    $this->grid = $grid;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->gridDiscovery->resetLayoutGrid($this->layoutDiscovery->getDefinition($this->layout), $this->gridDiscovery->getDefinition($this->grid));
    $this->messenger()->addStatus($this->t('The %grid grid was reset for %layout layout.', ['%grid' => $this->grid, '%layout' => $this->layout]));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
