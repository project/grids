<?php

namespace Drupal\grids\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;
use Drupal\grids\GridsPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles the grids' routes.
 */
class GridController extends ControllerBase {

  use RedirectDestinationTrait;

  /**
   * The Grids Plugin Manager.
   *
   * @var \Drupal\grids\GridsPluginManagerInterface
   */
  protected $gridsPluginManager;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * GridController constructor.
   *
   * @param \Drupal\grids\GridsPluginManagerInterface $gridsPluginManager
   *   The grids plugin manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   */
  public function __construct(GridsPluginManagerInterface $gridsPluginManager, ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $this->gridsPluginManager = $gridsPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.grids'),
      $container->get('config.factory')
    );
  }

  /**
   * Builds the grid list page.
   *
   * @return array
   *   Array of page elements to render.
   */
  public function list() {
    $definedGrids = [];
    $grids = $this->gridsPluginManager->getDefinitions();
    $destination = $this->getDestinationArray();

    $layouts = $this->gridsPluginManager->getGridLayouts();
    foreach ($layouts as $layout_id => $layout) {
      $definedGrids[] = [
        $layout->getLabel(),
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
      ];

      foreach ($grids as $grid) {
        $operations = [];
        if ($this->configFactory->getEditable("grids.{$layout_id}.{$grid->id()}")->isNew()) {
          $operations['create'] = [
            'title' => $this->t('Create'),
            'url' => Url::fromRoute('grids.grid.configure', array('layout' => $layout_id, 'grid' => $grid->id())),
            'query' => $destination,
          ];
        }
        else {
          $operations['configure'] = [
            'title' => $this->t('Configure'),
            'url' => Url::fromRoute('grids.grid.configure', array('layout' => $layout_id, 'grid' => $grid->id())),
            'query' => $destination,
          ];
          $operations['reset'] = [
            'title' => $this->t('Reset'),
            'url' => Url::fromRoute('grids.grid.reset', array('layout' => $layout_id, 'grid' => $grid->id())),
            'query' => $destination,
          ];
          $operations['delete'] = [
            'title' => $this->t('Delete'),
            'url' => Url::fromRoute('grids.grid.delete', array('layout' => $layout_id, 'grid' => $grid->id())),
            'query' => $destination,
          ];
        }

        $definedGrids[] = [
          NULL,
          $grid->getLabel(),
          $grid->id(),
          $grid->getDescription(),
          $grid->getCategory(),
          $grid->getProvider(),
          ['data' => ['#type' => 'operations', '#links' => $operations]],
        ];
      }

    }

    return [
      '#theme' => 'table',
      '#header' => [
        $this->t('Layout'),
        $this->t('Grid'),
        $this->t('ID'),
        $this->t('Description'),
        $this->t('Category'),
        $this->t('Provider'),
        $this->t('Operations'),
      ],
      '#rows' => $definedGrids,
      '#empty' => $this->t('No grids available.'),
      '#attributes' => [
        'class' => ['grids-list'],
      ],
    ];
  }

}
