<?php

namespace Drupal\grids\Routing;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Optional;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Converts grid plugin IDs in route parameters to grid plugin.
 *
 * To use it, add a `grid.type` key to the route parameter's options.
 * Its value is as follows:
 * @code
 * example.route:
 *   path: foo/{bar}
 *   options:
 *     parameters:
 *       bar:
 *         grids.grid_type:
 *           # Whether the conversion is enabled. Boolean. Optional. Defaults
 *           # to TRUE.
 *           enabled: TRUE
 * @endcode
 *
 * To use the default behavior, its value is as follows:
 * @code
 * example.route:
 *   path: foo/{bar}
 *   options:
 *     parameters:
 *       bar:
 *         grids.grid_type: {}
 * @endcode
 */
class GridTypeConverter implements ParamConverterInterface {

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return !empty($definition['layout_builder_tempstore']);
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    // If no section storage type is specified or if it is invalid, return.
    if (!isset($defaults['section_storage_type']) || !$this->sectionStorageManager->hasDefinition($defaults['section_storage_type'])) {
      return NULL;
    }

    $type = $defaults['section_storage_type'];
    // Load an empty instance and derive the available contexts.
    $contexts = $this->sectionStorageManager->loadEmpty($type)->deriveContextsFromRoute($value, $definition, $name, $defaults);
    // Attempt to load a full instance based on the context.
    if ($section_storage = $this->sectionStorageManager->load($type, $contexts)) {
      // Pass the plugin through the tempstore repository.
      return $this->layoutTempstoreRepository->get($section_storage);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function doConvert($plugin_type_id, array $converter_definition) {
    if ($this->pluginTypeManager->hasPluginType($plugin_type_id)) {
      return $this->pluginTypeManager->getPluginType($plugin_type_id);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  protected function getConverterDefinitionKey() {
    return 'grids.grid_type';
  }

  /**
   * {@inheritdoc}
   */
  protected function getConverterDefinitionConstraint() {
    return new Collection([
      'enabled' => new Optional(new Type('boolean')),
    ]);
  }

}
